import React from 'react'
import './Messages.less'

export default React.createClass({
  propTypes: {
    author: React.PropTypes.string,
    isBot: React.PropTypes.bool,
    messages: React.PropTypes.array
  },

  render: function () {
    let messages = this.props.messages.map(function (content, index) {
      return (
        <div className='message' key={index}>
          <div className='message__content'>{content}</div>
        </div>
      )
    })

    return (
      <li className={this.props.isBot ? 'post' : 'post post--from-right'}>
        <div className='post__author'>
          <img src={this.props.isBot ? '/images/' + this.props.author + '.jpg' : 'http://placehold.it/50x50'} alt='' width='50' height='50'/>
        </div>

        <div className='post__messages'>
          {messages}
        </div>
      </li>
    )
  }
})
