import React from 'react'
import { History } from 'react-router'
import MessageStore from '../../stores/MessageStore'
import webservice from '../../lib/webservice.js'
import './SplashScreen.less'

let SplashScreen = React.createClass({
  mixins: [History],

  getInitialState () {
    return { categories: [] }
  },

  componentWillMount () {
    let slug = MessageStore.getCurrentInterviewSlug()
    if (slug != null) {
      return this.history.pushState(null, `/interview/${slug}`)
    }
  },

  componentDidMount () {
    webservice.get().then(categories => {
      if (this.isMounted()) {
        this.setState({ categories })
      }
    })
  },

  render () {
    return (
      <main className='main-column'>
        <div className='splash-screen'>
          <div className='splash-screen__content'>
            <h2>What job would you like to train for?</h2>

            <ul className='list'>
              {this.state.categories.map(function (category, index) {
                return (
                  <li key={index}>
                    <h3><a href={`#/interview/${category.slug}`}>{category.title}</a></h3>
                    <small>Questions from: <a>john/front-end-interview</a></small>
                  </li>
                )
              })}
            </ul>
          </div>
        </div>
      </main>
    )
  }
})

export default SplashScreen
