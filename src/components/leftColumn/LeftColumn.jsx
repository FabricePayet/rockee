import React from 'react'
import './LeftColumn.less'

let LeftColumn = React.createClass({
  render: function () {
    return (
      <aside className='left-column'>
        <header className='left-column__header'>
          <a href='#'>
            <img src='http://placehold.it/80x60' alt='' style={{borderRadius: '6px'}}/>
          </a>
        </header>

        <nav className='nav' role='navigation'>
          <ul>
            <li>
              <a href='#' className='active'>
                <span className='icon-training'></span>
                Training
              </a>
            </li>

            <li>
              <a href='#'>
                <span className='icon-questionnaires'></span>
                Questionnaires
              </a>
            </li>

            <li>
              <a href='#'>
                <span className='icon-find'></span>
                Find a Job
              </a>
            </li>
          </ul>
        </nav>
      </aside>
    )
  }
})

export default LeftColumn
