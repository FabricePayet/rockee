import webservice from './webservice.js'

const BOT_NAME = 'Amy'

function Interview (interviewSlug) {
  let globals = {
    interviewSlug,
    messages: [],
    completed: false,
    questions: [],
    introduction: [
      [
        'Hey there! Welcome to Rockee - a service that helps developers training for job interviews.',
        'But first, what\'s your name? :)'
      ],

      [
        'Nice to meet you __username__! I am ' + BOT_NAME + ' and I\'ll have the pleasure to be your "virtual" interviewer.',
        'Before going any further, what kind of job would you like to train for?'
      ],

      [
        'Great, so from now on I\'ll (more or less) act as a recruiter looking for a "Front-End Developer".',
        'We\'ll go through a random serie of questions related to this field. Don\'t forget that it\'s supposed to be an oral interview so do your best to make your answers clear, short and simple...',
        'At the end of this session, you\'ll be able to review, save and share your answers. Please note that we do not store anything until you decide to do so.',
        'Ready to start?'
      ]
    ]
  }

  function _sendMessage (container) {
    let _messages = container[0]
    container.shift()
    globals.messages.push(_messages)
    return {
      author: BOT_NAME,
      isBot: true,
      messages: _messages,
      completed: globals.completed
    }
  }

  function _loadQuestions (callback) {
    webservice.get(globals.interviewSlug).then(questions => {
      globals.questions = questions.map(question => {
        return [question.question]
      })

      callback()
    })
  }

  function start () {
    return _sendMessage(globals.introduction)
  }

  function answer (response, callback) {
    if (globals.completed) {
      return setTimeout(function () {
        callback(null, {completed: globals.completed})
      }, 0)
    }

    if (globals.introduction.length > 0) {
      return setTimeout(function () {
        callback(null, _sendMessage(globals.introduction))
      }, 0)
    }

    if (globals.questions.length > 0) {
      let q = _sendMessage(globals.questions)
      if (globals.questions.length === 1) globals.completed = true
      return callback(null, q)
    }

    _loadQuestions(function (err) {
      if (err) return callback(err)
      return callback(null, _sendMessage(globals.questions))
    })
  }

  return { interviewSlug: globals.interviewSlug, start, answer }
}

export default Interview
