import reqwest from 'reqwest'

const API_ROOT = '/api/v1'

export default {
  get (slug) {
    let url = `${API_ROOT}/categories`
    if (slug != null) url += `/${slug}/questions`
    return reqwest({ url, method: 'get' })
  }
}
