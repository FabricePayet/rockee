import MessageDispatcher from '../dispatcher/MessageDispatcher'
import {ActionTypes} from '../constants/MessageConstants'

let MessageActions = {
  newInterview (interviewSlug) {
    MessageDispatcher.dispatch({
      type: ActionTypes.NEW_INTERVIEW,
      interviewSlug
    })
  },

  sendMessage (text) {
    MessageDispatcher.dispatch({
      type: ActionTypes.SEND_MESSAGE,
      message: {
        text: text,
        date: Date.now()
      }
    })
  }
}

export default MessageActions
