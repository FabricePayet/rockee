import MessageDispatcher from '../dispatcher/MessageDispatcher'
import MessageConstants from '../constants/MessageConstants'
import { EventEmitter } from 'events'
import assign from 'object-assign'
import Interview from '../lib/Interview'
import async from 'async'

const CHANGE_EVENT = 'change'
const DELAY_PER_CHARACTER = 40
const MAX_DELAY = 2600
const BREAK_DELAY = 600
const ActionTypes = MessageConstants.ActionTypes

let globals = {
  conversation: [],
  expectAnswer: true,
  expectUsername: true,
  isTyping: false,
  username: null,
  currentInterview: null
}

let MessageStore = assign({}, EventEmitter.prototype, {
  emitChange () {
    this.emit(CHANGE_EVENT)
  },

  addChangeListener (callback) {
    this.on(CHANGE_EVENT, callback)
  },

  removeChangeListener (callback) {
    this.removeListener(CHANGE_EVENT, callback)
  },

  isExpectingAnswer () {
    return globals.expectAnswer
  },

  getCurrentInterviewSlug () {
    return globals.currentInterview && globals.currentInterview.interviewSlug
  },

  getAll () {
    return {
      messages: globals.conversation,
      isTyping: globals.isTyping
    }
  },

  dispatchToken: MessageDispatcher.register(function (action) {
    switch (action.type) {
      case ActionTypes.NEW_INTERVIEW:
        // if currentInterview is not null
        // it means there's already an interview in progress
        if (globals.currentInterview != null) return

        globals.currentInterview = Interview(action.interviewSlug)
        addDelayedMessages(globals.currentInterview.start())
        break

      case ActionTypes.SEND_MESSAGE:
        if (!globals.expectAnswer) return

        if (globals.expectUsername) {
          let wordsArray = action.message.text.split(' ')
          let username = wordsArray[wordsArray.length - 1]
          globals.username = username.charAt(0).toUpperCase() + username.substring(1).toLowerCase()
          globals.expectUsername = false
        }

        globals.conversation.push({ author: globals.username, isBot: false, messages: [action.message.text] })

        return globals.currentInterview.answer(action.message, function (err, messages) {
          if (err) return console.error(err)
          addDelayedMessages(messages)
        })

      default:
        // do nothing
    }
  })
})

function _renderMessage (message) {
  return message.replace('__username__', globals.username)
}

function addDelayedMessages ({ author, messages, isBot, completed }) {
  globals.expectAnswer = false
  MessageStore.emitChange()

  async.eachSeries(
    messages,

    function (message, nextMessageCallback) {
      async.series([
        function (nextTaskCallback) {
          setTimeout(nextTaskCallback, BREAK_DELAY)
        },

        function (nextTaskCallback) {
          globals.isTyping = true
          MessageStore.emitChange()

          setTimeout(function () {
            let currentAuthor =
              globals.conversation.length
                ? globals.conversation[globals.conversation.length - 1].author
                : null

            if (currentAuthor !== author) {
              globals.conversation.push({ author, isBot, messages: [] })
            }

            let lastMessages = globals.conversation[globals.conversation.length - 1].messages
            lastMessages.push(_renderMessage(message))

            globals.isTyping = false

            MessageStore.emitChange()
            return nextTaskCallback()
          }, Math.min(message.length * DELAY_PER_CHARACTER, MAX_DELAY))
        }
      ], nextMessageCallback)
    },

    function () {
      globals.expectAnswer = !completed
      MessageStore.emitChange()
    }
  )
}

export default MessageStore
