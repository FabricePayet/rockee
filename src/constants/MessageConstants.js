import keyMirror from 'keymirror'

const CONSTANTS = {
  ActionTypes: keyMirror({
    NEW_INTERVIEW: null,
    RECEIVE_MESSAGE: null,
    SEND_MESSAGE: null
  })
}

export default CONSTANTS
