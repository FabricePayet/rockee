import './core/base.less'
import React from 'react'
import { Router, IndexRoute, Route } from 'react-router'

import Main from './components/main/Main.jsx'
import SplashScreen from './components/splashScreen/SplashScreen.jsx'
import Training from './components/training/Training.jsx'

React.render((
  <Router>
    <Route path='/' component={Main}>
      <IndexRoute component={SplashScreen} />
      <Route path='interview/:slug' component={Training} />
    </Route>
  </Router>
), document.getElementById('app'))
