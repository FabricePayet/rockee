import mongoose from 'mongoose'
import slugify from 'slug'

let slugCounterSchema = new mongoose.Schema({
  created: {
    type: Date,
    default: Date.now
  },
  modified: {
    type: Date,
    default: Date.now
  },
  collectionOwner: {
    type: String,
    require: true
  },
  value: {
    type: String,
    require: true
  },
  counter: {
    type: Number,
    default: 0
  }
})

slugCounterSchema.statics.slugify = function (str, collectionOwner, callback) {
  let slug = slugify(str, { lower: true })
  let query = { value: slug, collectionOwner }

  // look for an existing counter
  this.findOne(query, (err, doc) => {
    if (err) return callback(err)

    // there's already a document storing the counter
    if (doc) return getSlugWithSuffix(slug, doc, callback)

    // let's start storing a counter for this slug
    let Slug = this
    doc = new Slug(query)

    doc.save(function (err, resultDoc) {
      if (err) return callback(err)

      // now that the counter document is created
      // get its suffix value
      getSlugWithSuffix(slug, resultDoc, callback)
    })
  })

  function getSlugWithSuffix (slug, doc, callback) {
    // there's one more document with this given slug
    // -> increment the counter
    let counter = ++doc.counter

    doc.save(function (err) {
      if (err) callback(err)

      // if the counter === 1 we want front-end-developer
      // and not front-end-developer-1
      if (counter > 1) slug += '-' + counter

      callback(null, slug)
    })
  }
}

export default mongoose.model('SlugCounter', slugCounterSchema)
