import { Router } from 'express'
import Category from '../models/category'
import Question from '../models/question'

export default function (app) {
  let router = new Router()
  app.use('/api/v1/categories', router)

  router.route('/').get(function (req, res) {
    Category.find(function (err, categories) {
      if (err) {
        res.statusCode = 500
        return res.json({ error: err.message })
      }

      res.statusCode = 200
      res.json(categories)
    })
  })

  router.route('/:slug/questions').get(function (req, res) {
    let limit = req.query.limit || 10

    Question.findRandom({ categorySlug: req.params.slug }, limit, function (err, questions) {
      if (err) {
        res.statusCode = 500
        return res.json({ error: err.message })
      }

      res.statusCode = 200
      res.json(questions)
    })
  })
}
