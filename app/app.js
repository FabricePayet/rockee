import express from 'express'
import path from 'path'
import db from './db'
import router from './routes'

const PORT = process.env.PORT || 3000

let viewsDir = path.join(__dirname, 'views')
let publicDir = path.join(__dirname, '../public')

db.connect()

let app = express()
app.use(express.static(publicDir))
app.set('views', viewsDir)
app.set('view engine', 'jade')

router(app)

app.listen(PORT, function () {
  console.log(`Magic port is ${PORT}`)
})
