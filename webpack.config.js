var path = require('path')
var autoprefixer = require('autoprefixer')

module.exports = {
  entry: './src/index.jsx',

  output: {
    path: path.join(__dirname, '/public/js'),
    filename: 'app.min.js'
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: ['babel-loader']
      },

      {
        test: /\.json$/,
        loaders: ['json-loader']
      },

      {
        test: /\.less$/,
        loader: 'style!css!postcss!less'
      }
    ]
  },

  postcss: [
    autoprefixer({ browsers: ['last 2 versions'] })
  ]
}
